import React from 'react';
import style from './App.module.css';
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";
import Title from "./components/Title/Title";

// class App extends React.Component {

//   state = {
//     isOpenModal: false,
//   }

//   stating = {
//     isDeletemodal: true,
//   }

//   render() {
//     const { isOpenModal } = this.state;
//     const { isDeletemodal } = this.stating;

//     return (
//       <div className="App">

//         <button onClick={() => {
//           this.setState({ isOpenModal: true })
//         }}>OPEN</button>

//         {
//           isOpenModal &&
//           <div className='modal'>
//             <h2>Title</h2>
//             <button onClick={() => {
//               this.setState({ isOpenModal: false })
//             }}>CLOSE</button>
//           </div>
//         }

//       </div>
//     );
//   }

// }

// export default App;



export default class App extends React.Component {

  state = {
    firstModal: false,
    secondModal: false
  }

  firstModal = () => {
    this.setState({
      firstModal: !this.state.firstModal
    })
  }

  secondModal = () => {
    this.setState({
      secondModal: !this.state.secondModal
    })
  }

  render() {
    return (
      <div className={style.section_container}>
        <Title text={"WELCOME"} />
        <div className={style.section_button}>
          <Button
            onClick={this.firstModal}
            backgroundColor={"rgb(255,255,0)"}
            text={"Open first modal"}
          />

          <Button
            onClick={this.secondModal}
            backgroundColor={"rgb(0,0,255)"}
            text={"Open second modal"}
          />
        </div>
        {
          this.state.firstModal &&
          <Modal
            header={'First modal'}
            closeButton={true}
            isOpen={this.firstModal}
            modaltext={"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum explicabo ipsam iste magnam officiis quae?"}
            active={
              <div className={style.section_footer}>
                <Button text={"Ok"} />
                <Button text={"Cancel"} onClick={this.firstModal} />
              </div>}
          />
        }
        {
          this.state.secondModal &&
          <Modal
            header={'Second modal'}
            closeButton={true}
            isOpen={this.secondModal}
            modaltext={"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid amet aperiam, dolores ea eaque error exercitationem expedita hic nam natus nisi non omnis quae quas, suscipit voluptatem voluptatibus voluptatum? Provident."}
            active={
              <div className={style.section_footer}>
                <Button text={"React"} />
                <Button text={"Angular"} />
                <Button text={"Cancel"} onClick={this.secondModal} />
              </div>}
          />
        }
      </div>
    );
  }
}