import React from 'react';
import styles from './Title.module.css';

export default class Title extends React.Component {
    render() {
        return (
            <h2
                className={styles.title_text}>
                {this.props.text}
            </h2>
        )
    }
}