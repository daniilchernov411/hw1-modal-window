import React from 'react';
import styles from './Modal.module.css';

export default class Modal extends React.Component {
    render() {
        return (
            <div
                onClick={this.props.isOpen}
                className={styles.modal}>
                <div
                    onClick={event => event.stopPropagation()}
                    className={styles.modal__content}>

                    <div
                        className={styles.modal__header}>
                        <h3 className={styles.modal__title}>{this.props.header}</h3>
                        {this.props.closeButton && <span onClick={this.props.isOpen}>&#10761;</span>}
                    </div>

                    <div
                        className={styles.modal__nav}>
                        <p className={styles.modal__text}>
                            {this.props.modaltext}</p>
                    </div>
                    {this.props.active}
                </div>
            </div>
        )
    }

}